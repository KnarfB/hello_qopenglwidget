#include "openglwidget.h"
#include "model.h"

OpenGLWidget::OpenGLWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{

}

OpenGLWidget::~OpenGLWidget()
{
    vao->destroy();
    delete vao;
    vbo->destroy();
    delete vbo;
    texture->destroy();
    delete texture;
    // program inherited from QObject, will be destroyed automatically
}



void OpenGLWidget::initializeGL()
{
    logger = new QOpenGLDebugLogger(this);
    logger->initialize();
    logger->startLogging();
    connect(logger, &QOpenGLDebugLogger::messageLogged, this, &OpenGLWidget::messageLogged);

    initializeOpenGLFunctions();

    qDebug() << reinterpret_cast<const char*>(glGetString(GL_VENDOR));
    qDebug() << reinterpret_cast<const char*>(glGetString(GL_RENDERER));
    qDebug() << reinterpret_cast<const char*>(glGetString(GL_VERSION));
    qDebug() << format();

    glClearColor( 0.0, 0.0, 0.4, 1.0 );
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_POLYGON_SMOOTH);
    //glEnable(GL_CULL_FACE);

    program = new QOpenGLShaderProgram(this);

    if( !program->addShaderFromSourceFile( QOpenGLShader::Vertex, ":/glsl/vertexShaderSource.txt" ) ) {
        qDebug() << program->log();
        exit(EXIT_FAILURE);
    }

    if( !program->addShaderFromSourceFile( QOpenGLShader::Fragment, ":/glsl/fragmentShaderSource.txt" ) ) {
        qDebug() << program->log();
        exit(EXIT_FAILURE);
    }

    if( !program->link() ) {
        qDebug() << program->log();
        exit(EXIT_FAILURE);
    }

    program->bind();

    //model = new Model("cube.txt");
    //model = new Model("knot.txt");
    //model = new Model("sphere.txt");
    model = new Model("teapot.txt");

    vao = new QOpenGLVertexArrayObject;
    vao->create();
    vao->bind();

    vbo = new QOpenGLBuffer;
    vbo->create();
    vbo->bind();
    vbo->setUsagePattern(QOpenGLBuffer::StaticDraw);
    vbo->allocate( model->getData(), model->size() );

    // this encapsulates glGetAttribLocation and glEnableVertexAttribArray
    program->enableAttributeArray("vertexPosition_modelspace");
    program->enableAttributeArray("vertexUV");
    program->enableAttributeArray("vertexNormal_modelspace");

    // this encapsulates glGetAttribLocation and glVertexAttribPointer
    program->setAttributeBuffer("vertexPosition_modelspace", GL_FLOAT, 0,                   3, 8 * sizeof(GLfloat));
    program->setAttributeBuffer("vertexUV",                  GL_FLOAT, 3 * sizeof(GLfloat), 2, 8 * sizeof(GLfloat));
    program->setAttributeBuffer("vertexNormal_modelspace",   GL_FLOAT, 5 * sizeof(GLfloat), 3, 8 * sizeof(GLfloat));

    vbo->release();
    vao->release();
    program->release();

    texture = new QOpenGLTexture(QImage(":/glsl/CE.png").mirrored());

    connect( &timer, SIGNAL(timeout()), this, SLOT(update()) );
    timer.setInterval(0);
    timer.start();

    last_ms = QTime::currentTime().msecsSinceStartOfDay();
}

void OpenGLWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    int ms = QTime::currentTime().msecsSinceStartOfDay();
    qDebug() << ms - last_ms;

    float x = sin(ms/1000.);
    float y = cos(ms/1000.);

    QMatrix4x4 model;   // Wo ist unser Modell in der Welt?
    QMatrix4x4 view;    // Was ist die Kamerapose in der Welt?   camera extinsics
    QMatrix4x4 proj;    // Wie bildet unsere Kamera ab?         camera intrinsics

    view.lookAt(
        QVector3D( x*1.5, y*1.5, 1.0 ),     // eye point
        QVector3D( 0.0, 0.0, 0.0 ),     // focal point
        QVector3D( 0.0, 0.0, 1.0 )      // up vector
    );

    proj.perspective( 30.0f, (float)w/h, 0.1f, 100.0f ); // code could be moved to resizeGL

    program->bind();    // this encapsulates glUseProgram

    program->setUniformValue("M", model );
    program->setUniformValue("V", view );
    program->setUniformValue("MVP", proj * view * model );

    program->setUniformValue("LightPosition_worldspace", QVector3D(1.0f,1.0f,1.0f) );
    program->setUniformValue("LightColor", QVector3D(0.5f,0.5f,0.5f) );
    program->setUniformValue("LightPower", 20.0f );

    texture->bind();
    vao->bind();
    glDrawArrays(GL_TRIANGLES, 0, this->model->vertices() );
    vao->release();
    texture->release();

    program->release();

    last_ms = ms;
}

void OpenGLWidget::resizeGL(int w, int h)
{
    //glViewport( 0,0,w,h );
    this->w = w;
    this->h = h;
}

void OpenGLWidget::messageLogged(const QOpenGLDebugMessage &message)
{
    qDebug() << message;
}
