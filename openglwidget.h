#ifndef OPENGLWIDGET_H
#define OPENGLWIDGET_H

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

#include "model.h"

class OpenGLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    OpenGLWidget(QWidget *parent = nullptr);
    ~OpenGLWidget();

public slots:
    void messageLogged(const QOpenGLDebugMessage &message);

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int w, int h) override;

private:

    QOpenGLVertexArrayObject *vao;
    QOpenGLBuffer *vbo;
    QOpenGLTexture *texture;
    QOpenGLShaderProgram *program;
    QOpenGLDebugLogger *logger;

    Model *model;

    int w;
    int h;

    QTimer timer;
    int last_ms;
};

#endif // OPENGLWIDGET_H
