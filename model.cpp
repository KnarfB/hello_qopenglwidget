#include "model.h"

#include <QtCore>
#include <QtGui>
#include <QtWidgets>

Model::Model(QObject *parent) : QObject(parent)
{

}

Model::Model(const QString fileName, QObject *parent)
{
    QFile f(fileName);
    if( !f.open(QIODevice::ReadOnly) ) {
        QMessageBox::warning( nullptr, tr("Model"), tr("Failed to open model file") );
    }
    char lineBuffer[80];
    f.readLine(lineBuffer,sizeof(lineBuffer));
    len = QString(lineBuffer).toULong();
    data = new float[len];
    for( unsigned long i=0; i<len; ++i ) {
        f.readLine(lineBuffer,sizeof(lineBuffer));
        data[i] = QString(lineBuffer).toFloat();
    }
    f.close();
}

float *Model::getData() const
{
    return data;
}

unsigned int Model::size() const
{
    return len*sizeof(float);
}

unsigned int Model::vertices() const
{
    return len / 8;
}
