#include "mainwindow.h"

#include <QApplication>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSurfaceFormat format;
    format.setVersion(3,3);                         // major.minor version
    format.setProfile(QSurfaceFormat::CoreProfile); // no legacy compatability
    format.setSamples(2);                           // for multisampling
    format.setDepthBufferSize(24);                  // use depth buffer
    format.setOption(QSurfaceFormat::DebugContext); // for debug logging
    QSurfaceFormat::setDefaultFormat(format);

    MainWindow w;
    w.show();
    return a.exec();
}
