#ifndef MODEL_H
#define MODEL_H

#include <QObject>

class Model : public QObject
{
    Q_OBJECT
public:
    explicit Model(QObject *parent = nullptr);

    Model(const QString fileName, QObject *parent = nullptr);

    // data order is location (x y z), texCoord (u, v), normal (nx, ny, nz)
    float *getData() const;

    // size of vertex array in bytes
    unsigned int size() const;

    // number of vertices
    unsigned int vertices() const;

signals:

private:
   float *data;
   unsigned long len;
};

#endif // MODEL_H
